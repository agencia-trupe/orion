$(function() {
  $('#carousel').carouFredSel({
          width: '100%',
          items: {
            visible: 3,
            start: -1
          },
          auto: {
            play:false,
          },
          scroll: {
            items: 1,
            duration: 500,
            timeoutDuration: 3000
          },
          prev: '.prev',
          next: '.next',
          pagination: {
            container: '#pager',
            deviation: 1
          }
        });
  $(window).scroll(function(){
    if ($(this).scrollTop() > 200) {
      $('#navigation').fadeIn();
    } else {
      $('#navigation').fadeOut();
    }
  });
  //Click event to scroll to top
  $('.scrollToTop').click(function(){
    $('html, body').animate({scrollTop : 0},800);
    return false;
  });
  $("#accordion").accordion({ 
    autoHeight: true,
    collapsible: true,
    heightStyle: "content"
  });    
});

function runSlider(){
      //altera o resultado do array retornado em serve_slides
    function returnAsset(item){
      return 'assets/img/slides/' + item;
    }
    //Retorna o link para o projeto pai do slide atual
    function getSlideLink(id){
      return location.protocol + '//' + location.hostname + 'previa/projetos/detalhe/' + id;
    }
    //Iinicia o full screen slideshow
      $.getJSON('index.php/home/serve_slides', function(data) {
         $.backstretch(data.map(returnAsset), {
          duration:300000000,
          fade:2000 //was 1500
        });
      });
      slider = $('.bxslider').bxSlider({
          'auto':true,
          'pager':false,
          'speed': 2000, //was 1500
          'mode':'fade',
          'nextSelector':'#slide_right',
          'nextText':'',
          'prevSelector':'#slide_left',
          'prevText':'',
          onSlideBefore: function($slideElement, oldIndex, newIndex){
            if(( oldIndex + 1 ) < newIndex )
            {
              $("body").data("backstretch").prev();
              slider.startAuto();
            }
            else{
              if(oldIndex < newIndex || oldIndex > (newIndex + 1)){
              $("body").data("backstretch").next();
              slider.startAuto();
              } 
              else {
                $("body").data("backstretch").prev();
                slider.startAuto();
              }
            }
            
          }
        });
    }
/*
$( "#accordion" ).bind( "accordionchangestart", function(event, ui) {
});
*/
//Verifica o suporte ao HTML Placeholder - home/contato
jQuery(function() {
  jQuery.support.placeholder = false;
  test = document.createElement('input');
  if('placeholder' in test) jQuery.support.placeholder = true;
});

//Hack para navegadores que não oferecem suporte ao HTML5 Placeholder - home/contato
$(function() {
  if(!$.support.placeholder) { 
    var active = document.activeElement;
    $(':text').focus(function () {
      if ($(this).attr('placeholder') != '' && $(this).val() == $(this).attr('placeholder')) {
        $(this).val('').removeClass('hasPlaceholder');
      }
    }).blur(function () {
      if ($(this).attr('placeholder') != '' && ($(this).val() == '' || $(this).val() == $(this).attr('placeholder'))) {
        $(this).val($(this).attr('placeholder')).addClass('hasPlaceholder');
      }
    });
    $(':text').blur();
    $(active).focus();
    $('form').submit(function () {
      $(this).find('.hasPlaceholder').each(function() { $(this).val(''); });
    });
  }
});

//Prepara a requisição ajax do tipo POST para enviar os dados do formulário de
//contato - contato
$(function() {
 $('#contato-form').submit(function() {
    $('#message').html('Enviando...'); 
    var form_data = {
      nome : $('.nome_form').val(),
      email : $('.email_form').val(),
      telefone : $('.telefone_form').val(),
      mensagem : $('.mensagem_form').val(),
       ajax : '1'
    };
    $.ajax({
      url: location.protocol + "//" + location.host + "/contato/ajax_check",
      type: 'POST',
      async : false,
      data: form_data,
      success: function(msg) {
        alert(msg);
        $('.nome_form, .email_form, .telefone_form, .mensagem_form').val('');
      }
    });
    return false;
  });
}); 

