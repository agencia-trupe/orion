<div class="span9">
    <?php if($this->session->flashdata('error') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('error'); ?>
    </div>
    <?php endif; ?> 
    <?php if($this->session->flashdata('success') != NULL): ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('success'); ?>
    </div>
    <?php endif; ?>
    <div class="row-fluid">
        <div class="span6">
            <legend>Galeria <a href="<?php echo site_url('painel/cadastra/' . $tipo); ?>" class="btn btn-small">Adicionar imagens</a></legend>
        </div>
        <?php 
            if (isset($update_success))
            {
                echo '<div class="span8"><div class="alert alert-success">'.$update_success.$today.'às '.$time.'</div></div>';
            }
        ?>
    </div>
  <?php if(isset($result)): ?>
     <div class="thumbs">
        <?php foreach($result as $img): ?>
            <div class="thumb">
                <img src="<?php echo base_url(); ?>assets/img/fotos/thumbs/<?php echo $img->imagem; ?>" alt="<?php echo $img->titulo; ?>" >
                <span><?php echo $img->titulo; ?></span>
                <a href="<?php echo site_url('painel/edita/' . $img->id); ?>">Editar</a>
                <a id="removelink" href="<?php echo site_url('adminfotos/apaga/' . $img->categoria . '/' . $img->id); ?>">Apagar</a>
            </div>
        <?php endforeach; ?>
        <div class="clearfix"></div>
        <?php echo $this->pagination->create_links(); ?>
     </div> 
  <?php endif; ?>
        

</div><!--/span-->