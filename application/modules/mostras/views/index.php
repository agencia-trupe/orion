<div class="conteudo projetos">
    <?php if ( isset($mostras) ):
          foreach ( $mostras as $mostra ): ?>
        <a href="<?=site_url( 'mostras/detalhe/' . $mostra->id ); ?>" class="projeto-box">
            <img src="<?=base_url( 'assets/img/projetos/capas/' . $mostra->capa ); ?>" 
             alt="<?=$mostra->titulo; ?>">
            <span class="titulo"><?=$mostra->titulo; ?></span>
            <div class="lupa"></div>
        </a>
    <?php endforeach; 
        endif; ?>
    <div class="clearfix"></div>
</div>
<div class="clearfix"></div>