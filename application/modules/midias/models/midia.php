<?php
class Midia extends Datamapper
{
    var $table = 'midia';

    function get_all()
    {
        $midia = new Midia();
        $midia->get();
        $arr = array();
        foreach( $midia->all as $midia )
        {
            $arr[] = $midia;
        }
        if( sizeof( $arr ) )
        {
            return $arr;
        }
        return NULL;
    }
    function get_conteudo( $id )
    {
        $midia = new Midia();
        $midia->where( 'id', $id )->get();
        if( $midia->exists() ){
            return $midia;
        }
        return NULL;
    }

    function get_max()
    {
        $clipping = new Midia();
        $clipping->select_max('id')->get();
        return $clipping;
    }


    function get_min()
    {
        $clipping = new Midia();
        $clipping->select_min('id')->get();
        return $clipping;
    }


    function get_next($id)
    {
        $min = $this->get_min();
        $next = new Midia();

        $next->order_by('id', 'ASC')
             ->where('id > ', $id)
             ->select('id')
             ->get(1);

        if( !$next->exists() ) return $min->id;

        return $next->id;
    }

    function get_prev($id)
    {
        $max = $this->get_max();
        $prev = new Midia();

        $prev->order_by('id', 'DESC')
             ->where('id < ', $id)
             ->select('id')
             ->get(1);

        if( !$prev->exists() ) return $max->id;

        return $prev->id;
    }

    function get_related( $id, $position )
    {
        $midia = new Midia();
        switch ( $position ) {
            case 'prev':
                $midia->where( 'id <', $id);
                break;
            case 'next':
                $midia->where( 'id >', $id);
                break;
        }
        $midia->get(1);
        if($midia->exists())
        {
            return $midia->id;
        }
        return FALSE;
    }

    function insert($dados)
    {
        $midia = new Midia();
        foreach ($dados as $key => $value)
        {
            $midia->$key = $value;
        }
        $midia->created = time();
        $insert = $midia->save();
        if($insert)
        {
            return TRUE;
        }
        return FALSE;
    }

    function change($dados)
    {
        $midia = new Midia();
        $midia->where('id', $dados['id']);
        $update_data = array();
        foreach ($dados as $key => $value)
        {
            $update_data[$key] = $value;
        }
        $update_data['updated'] = time();
        $update = $midia->update($update_data);
        if($update)
        {
            return TRUE;
        }
        return FALSE;
    }

    function apaga($id)
    {
        $servico = new Midia();
        $servico->where('id', $id)->get();
        if($servico->delete())
        {
            return TRUE;
        }
        return FALSE;
    }

    function ordena($dados)
    {
        $result = array();
        foreach($dados as $key => $value)
        {
            $categoria = new Midia();
            $categoria->where('id', $value);
            $update_data = array(
                'ordem' => $key
                );
            if($categoria->update($update_data))
            {
                $result[] = $value;
            }
        }
        if(sizeof($result))
        {
            return TRUE;
        }
        else
        {
            return FALSE;
        }
    }
}