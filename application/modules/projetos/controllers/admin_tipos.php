<?php
class Admin_tipos extends MX_Controller
{
    var $data;
    public function __construct()
    {
        parent::__construct();
        $this->data['module'] = 'projetos';
        $this->load->model('projetos/tipo');
    }
    public function index()
    {
        $this->lista();
    }

    public function lista()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Celito Gonzalez - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['tipos'] = $this->tipo->get_all();
            $this->data['conteudo'] = 'projetos/admin_lista_tipos';
            $this->load->view('start/template', $this->data);
        }
    }

    public function editar($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Celito Gonzalez - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'editar';
            $this->data['tipo'] = $this->tipo->get_conteudo($id, 'id');
            $this->data['conteudo'] = 'projetos/admin_edita_tipos';
            $this->load->view('start/template', $this->data);
        }
    }

    public function cadastrar()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Celito Gonzalez - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $this->data['acao'] = 'cadastrar';
            $this->data['conteudo'] = 'projetos/admin_edita_tipos';
            $this->load->view('start/template', $this->data);
        }
    }

    public function processa_cadastro()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Celito Gonzalez - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('tipos'))
            {
                $this->data['acao'] = 'cadastrar';
                $this->data['conteudo'] = 'projetos/admin_edita_tipos';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                $this->load->helper('blog');
                $post['slug'] = get_slug($post['nome']);
                $post['created'] = time();
                if($this->tipo->insert($post))
                {
                    $this->session->set_flashdata('success', 'Registro adicionado com sucesso');
                    redirect('painel/tipos');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível adicionar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/tipos/');
                }
            }
        }
    }

    public function processa()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Celito Gonzalez - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            if(!$this->form_validation->run('tipos'))
            {
                $this->data['acao'] = 'editar';
                $this->data['tipo'] = $this->tipo->get_conteudo($this->input->post('id'));
                $this->data['fotos'] = $this->foto->get_tipo($this->input->post('id'));
                $this->data['conteudo'] = 'projetos/admin_edita_tipos';
                $this->load->view('start/template', $this->data);
            }
            else
            {
                $post = array();
                foreach($_POST as $key => $value)
                {
                    $post[$key] = $value;
                }
                if($this->tipo->change($post))
                {
                    $this->session->set_flashdata('success', 'Registro alterado com sucesso');
                    redirect('painel/tipos');
                }
                else
                {
                    $this->session->set_flashdata('error', 'Não foi possível alterar o registro.
                        Tente novamente ou entre em contato com o suporte');
                    redirect('painel/tipos/edita/' . $post['id']);
                }
            }
        }
    }

    public function deleta_tipo($id)
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Celito Gonzalez - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $apaga = $this->tipo->apaga($id);
            if($apaga)
            {
                $this->session->set_flashdata('success', 'Registro removido com sucesso');
                redirect('painel/tipos');
            }
            else
            {
                $this->session->set_flashdata('error', 'Não foi possível remover o registro.
                    Tente novamente ou entre em contato com o suporte');
                redirect('painel/tipos/');
            }
        }
    }

    /**
     * Reordena os tipos de produtos para a exibição
     * @return void status do processamento
     */
    public function sort_tipos()
    {
        if (!$this->tank_auth->is_logged_in()) 
        {
            $this->session->set_userdata('bounce_uri',
                $this->uri->uri_string());
            $this->data['main_content'] = 'system/mustLogin';
            $this->data['title'] = 'Celito Gonzalez - Painel de Controle';
            $this->load->view('start/templatenonav', $this->data);
        }
        else
        {
            $itens = $this->input->post('tipo');
            if ($itens)
            {
                $ordenar = $this->tipo->ordena($itens);
                if($ordenar)
                {
                    echo 'Ordenado';
                }
                else
                {
                    echo 'Erro!';
                }
                /*foreach($items as $key => $value) 
                {           
                    // Use whatever SQL interface you're using to do
                    // something like this:
                    // UPDATE image_table SET sort_order = $key WHERE image_id = $value
                }*/
            } 
            else 
            {
              echo 'Erro!';
            }
        }
    }
}