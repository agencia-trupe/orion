<div id="direita">
   <?php if($this->session->flashdata('message') != NULL): ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('message'); ?>
    </div>
    <?php endif; ?>
  <div class="alert alert-info span6">
      <p>Para recuperar sua senha, digite seu email ou nome de usuário no campo abaixo e clique em "recuperar senha".</p>
    </div>
  <div class="page-content">
    <?php
$login = array(
	'name'	=> 'login',
	'id'	=> 'login',
	'value' => set_value('login'),
	'maxlength'	=> 80,
	'size'	=> 30,
);
if ($this->config->item('use_username', 'tank_auth')) {
	$login_label = '';
} else {
	$login_label = 'Email';
}
?>
    
<?php echo form_open($this->uri->uri_string()); ?>
<table>
	<tr>
		<td><?php echo form_label($login_label, $login['id']); ?></td>
		<td><?php echo form_input($login); ?></td>
		<td style="color: red;"><?php echo form_error($login['name']); ?><?php echo isset($errors[$login['name']])?$errors[$login['name']]:''; ?></td>
	</tr>
</table>
<?php echo form_submit('reset', 'Recuperar Senha', 'class="btn btn-warning"'); ?>
<?php echo form_close(); ?>

  </div>
</div>						
                <div class="clearfix"></div>

		</div>
                <div id="container-base"></div>
		
