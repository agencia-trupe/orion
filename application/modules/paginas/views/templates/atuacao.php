<div class="conteudo pagina <?=$pagina; ?>">
    <div class="inverse-wrapper">
        <div class="pagina-texto inverse <?=(!is_null($dados_pagina->imagem)) ? 'has-image' : ''; ?>">
            <div class="pagina-texto-wrapper">
                <?=$dados_pagina->texto; ?>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="pagina-accordion">
        <?=Modules::run('servicos/parcial'); ?>
    </div>  
    <? if(!is_null($dados_pagina->imagem)): ?>
        <div class="pagina-imagem right">
            <img src="<?=base_url(); ?>assets/img/paginas/<?=$dados_pagina->imagem; ?>" alt="<?=$dados_pagina->titulo; ?>">
        </div>
    <? endif; ?>
    <div class="clearfix"></div>
</div>