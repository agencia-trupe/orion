<?php
class Paginas extends MX_Controller
{
    var     $data;

    public function __construct()
    {
        parent::__construct();
        $this->load->model('paginas/pagina');
    }
    public function index()
    {
        $this->view();
    }
    public function view($slug)
    {
        //busca os dados de uma página cujo slug foi passado como parâmetro
        $dados_pagina = $this->pagina->get_conteudo($slug, $type = 'slug');
        if(!is_null($dados_pagina))
        {
            //Variável com os dados da página a ser enviada para a view
            $this->data['dados_pagina'] = $dados_pagina;
            //Carrega a biblioteca de SEO e a inicializa.
            $seo = array(
                'title' => $dados_pagina->titulo,
                'description' => $dados_pagina->description,
                );
            $this->load->library('seo', $seo);
            //Define a variável de página para o menu
            $this->data['pagina'] = $slug;
            //Define a view utilizada | Template dinâmica ou fixa.
            if(!empty($dados_pagina->template))
            {
                $this->data['conteudo'] = 'paginas/templates/' . $dados_pagina->template;
            }
            else
            {
                $this->data['conteudo'] = 'paginas/templates/index';
            }
            //Carrega a view especificada como parâmetro e exibe a página
            $this->load->view('layout/template', $this->data);
        }
        else
        {
            show_404();
        }
    }

    public function insert()
    {
        $dados = array();
        $dados['titulo'] = 'Atuação';
        $dados['slug'] = $this->_generate_slug('Atuação', 70);
        $dados['texto'] = '<p>Página <b>Atuação</b></p>';
        $dados['ativo'] = 1;
        $dados['template'] = 'atuacao';
        $insert = $this->pagina->insert($dados);
        if($insert)
        {
            echo 'Ok!';
        }
        else
        {
            echo 'Nooooo!';
        }
    }

    private function _generate_slug($phrase, $maxLength)
    {
        $result = strtolower($phrase);
     
        $result = preg_replace("/[^a-z0-9\s-]/", "", $result);
        $result = trim(preg_replace("/[\s-]+/", " ", $result));
        $result = trim(substr($result, 0, $maxLength));
        $result = preg_replace("/\s/", "-", $result);
     
        return $result;
    }
}